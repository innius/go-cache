package cache

import "bitbucket.org/innius/go-cache/redis"

type Cache interface {
	SetStringValue(string, string) error

	GetStringValue(string) (chan string, chan error)

	GetStringValueSync(string) (string, error)

	// repository methods
	GetSync(key string, v interface{}) error
	Get(key string, v interface{}) (chan bool, chan error)
	MGetSync(keys []string, vv interface{}) error
	MGet(keys []string, vv interface{}) (chan bool, chan error)

	Put(key string, v interface{}) error
	PutWithExpiration(key string, v interface{}, expire int) error
	MPut(keys []string, vv interface{}) error
	MPutWithExpiration(keys []string, vv interface{}, expire int) error

	ExistsSync(key string) (bool, error)
	Exists(key string) (chan bool, chan error)
	MExistsSync(keys []string) ([]bool, error)
	MExists(keys []string) (chan bool, chan error)

	PutGeo(key string, geo rediscache.RedisGeo) error
	GetGeo(key string, members []string) ([]*rediscache.RedisGeo, error)
	InGeoRange(key string, longitude float64, latitude float64, radius float64) ([]*rediscache.RedisGeo, error)

	PutInSet(key string, values []string) error
	GetSetMembers(key string) ([]string, error)

	PutInSortedSet(key string, scores map[uint64]string) error
	RangeSortedSet(key string, scoreLow uint64, scoreHigh uint64) ([]string, error)

	KeysSync(filter string) ([]string, error)
	Keys(filter string) (chan []string, chan error)

	// check if the cache is available
	Ping() error

	// clear the whole cache. Use with care!
	Clear() error
}
