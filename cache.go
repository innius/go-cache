package cache

import (
	"bitbucket.org/innius/go-cache/redis"
	"github.com/garyburd/redigo/redis"

)

// NewCacheClient returns a new cache client for a given aws cluster
func NewCacheClient(cluster string, ttl int) Cache {
	return rediscache.NewCache(cluster, ttl)
}

// NewCacheClient returns a new cache client for a given aws cluster
func NewLocalCacheClient(server string, port int64, ttl int) Cache {
	return rediscache.NewRedisCache(server, port, ttl)
}

func NewRedisConnectionPool(cluster string) *redis.Pool {
	return rediscache.NewPool(func() (string, int64, error) {
		endpoint, err := rediscache.GetClusterEndpoint(cluster)
		if err != nil {
			return "", 0, err
		}
		return *endpoint.Address, *endpoint.Port, nil
	})
}
