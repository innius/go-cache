module bitbucket.org/innius/go-cache

go 1.20

require (
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
	github.com/aws/aws-sdk-go v1.16.0
	github.com/garyburd/redigo v1.6.0
	github.com/pkg/errors v0.8.0
	github.com/stretchr/testify v1.9.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20160202185014-0b12d6b521d8 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/smartystreets/goconvey v1.8.1 // indirect
	github.com/stretchr/objx v0.5.2 // indirect
	golang.org/x/net v0.22.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
