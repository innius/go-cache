package rediscache

import (
	"net"
	"strconv"
	"time"

	"github.com/afex/hystrix-go/hystrix"
	"github.com/garyburd/redigo/redis"
)

type connFunc func() (string, int64, error)

// NewCache returns a new redis cache instance based on a provided cluster name
func NewCache(cluster string, ttl int, cfg ...hystrix.CommandConfig) *RedisCache {
	rc := &RedisCache{
		Pool: NewPool(func() (string, int64, error) {
			endpoint, err := GetClusterEndpoint(cluster)
			if err != nil {
				return "", 0, err
			}
			return *endpoint.Address, *endpoint.Port, nil
		}),
		ttl: ttl,
	}
	rc.configureHystrix(cfg...)

	return rc
}

func NewRedisCache(server string, port int64, ttl int, cfg ...hystrix.CommandConfig) *RedisCache {
	rc := &RedisCache{
		Pool: NewPool(func() (string, int64, error) {
			return server, port, nil
		}),
		ttl: ttl,
	}
	rc.configureHystrix(cfg...)

	return rc
}

type RedisCache struct {
	Pool *redis.Pool
	ttl  int
}

const (
	connectTimeout = 1 * time.Second
)

func NewPool(cf connFunc) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			host, port, err := cf()

			if err != nil {
				return nil, err
			}
			srv := net.JoinHostPort(host, strconv.FormatInt(port, 10))

			c, err := redis.Dial("tcp", srv, redis.DialConnectTimeout(connectTimeout))
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}
