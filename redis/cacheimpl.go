package rediscache

import (
	// "fmt"
	"reflect"

	"github.com/afex/hystrix-go/hystrix"
	"github.com/garyburd/redigo/redis"
	"github.com/pkg/errors"
	"strconv"
)

const (
	redisCommand = "cache_redis"

	DEFMAXCC   = 1000
	DEFTIMEOUT = 5000 // 5 seconds
)

// simple configuration of hystrix, with only maxcc and timeout for all commands at once.
// may need to change if the go-cache package is consumed by more clients
func (c *RedisCache) configureHystrix(cfg ...hystrix.CommandConfig) {
	maxcc := DEFMAXCC
	timeout := DEFTIMEOUT
	if len(cfg) > 0 {
		maxcc = cfg[0].MaxConcurrentRequests
		timeout = cfg[0].Timeout
	}
	hystrix.Configure(map[string]hystrix.CommandConfig{
		redisCommand: hystrix.CommandConfig{MaxConcurrentRequests: maxcc, Timeout: timeout},
	})
}

func (c *RedisCache) Ping() error {
	conn := c.Pool.Get()
	defer conn.Close()
	_, err := conn.Do("PING")
	return errors.Wrap(err, "Could not ping the redis cluster.")
}

func (c *RedisCache) SetStringValue(key string, value string) error {
	// wrap in synchronous hystrix command
	return hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		conn.Send("MULTI")
		conn.Send("SET", key, value)
		conn.Send("EXPIRE", key, c.ttl)
		_, err := conn.Do("EXEC")

		if err != nil {
			return errors.Wrapf(err, "Could not set %s to cache.", key)
		}
		return nil
	}, nil)
}

func (c *RedisCache) GetStringValueSync(key string) (string, error) {
	oc, ec := c.GetStringValue(key)

	select {
	case output := <-oc:
		return output, nil

	case err := <-ec:
		return "", err
	}
}

func (c *RedisCache) GetStringValue(key string) (chan string, chan error) {
	output := make(chan string, 1)

	ec := hystrix.Go(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		v, err := conn.Do("GET", key)
		if err != nil {
			return err
		}

		if v == nil {
			output <- ""
			return nil
		}
		val, err := redis.String(v, nil)
		if err != nil {
			return err
		}

		output <- val
		return nil
	}, nil)

	return output, ec
}

func (c *RedisCache) GetSync(key string, v interface{}) error {
	rc, ec := c.Get(key, v)

	select {
	case err := <-ec:
		return err
	case <-rc:
		return nil
	}
}

func (c *RedisCache) Get(key string, v interface{}) (chan bool, chan error) {
	rc := make(chan bool, 1)

	ec := hystrix.Go(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		values, err := redis.Values(conn.Do("HGETALL", key))
		if err != nil {
			return errors.Wrapf(err, "redis: HGETALL/values for key %v failed", key)
		}
		if len(values) == 0 {
			rc <- false
			return nil
		}
		err = redis.ScanStruct(values, v)
		if err != nil {
			return errors.Wrapf(err, "redis: scan values for key %v failed", key)
		}
		rc <- true
		return nil
	}, nil)

	return rc, ec
}

func (c *RedisCache) MGetSync(keys []string, v interface{}) error {
	rc, ec := c.MGet(keys, v)

	for i := 0; i < len(keys); i++ {
		select {
		case err := <-ec:
			return err
		case <-rc:
		}
	}
	return nil
}

func (c *RedisCache) MGet(keys []string, v interface{}) (chan bool, chan error) {
	rc := make(chan bool, len(keys))
	ec := hystrix.Go(redisCommand, func() error {
		var err error
		conn := c.Pool.Get()
		defer conn.Close()

		for _, k := range keys {
			err = conn.Send("HGETALL", k)
			if err != nil {
				return errors.Wrap(err, "redis: HGETALL failed")
			}
		}
		err = conn.Flush()
		if err != nil {
			return errors.Wrap(err, "redis: FLUSH failed")
		}
		for i := 0; i < len(keys); i++ {
			values, err := redis.Values(conn.Receive())
			if err != nil {
				return errors.Wrap(err, "redis: RECEIVE/values failed")
			}
			if len(values) == 0 {
				rc <- false
				continue
			}
			err = redis.ScanStruct(values, getPointer(v, i))
			if err != nil {
				return errors.Wrap(err, "redis: scan values failed")
			}
			rc <- true
		}
		return nil
	}, nil)

	return rc, ec
}
func (c *RedisCache) PutWithExpiration(key string, v interface{}, ttl int) error {
	// wrap in synchronous hystrix command
	return hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		var args redis.Args
		args = args.Add(key)
		args = args.AddFlat(v)

		conn.Send("MULTI")
		conn.Send("HMSET", args...)
		conn.Send("EXPIRE", key, ttl)
		_, err := conn.Do("EXEC")
		if err != nil {
			return errors.Wrapf(err, "redis: HMSET/EXPIRE for key %v failed", key)
		}
		return nil
	}, nil)
}

func (c *RedisCache) Put(key string, v interface{}) error {
	// wrap in synchronous hystrix command
	return hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		var args redis.Args
		args = args.Add(key)
		args = args.AddFlat(v)

		_, err := conn.Do("HMSET", args...)
		if err != nil {
			return errors.Wrapf(err, "redis: HMSET for key %v failed", key)
		}
		return nil
	}, nil)
}

func (c *RedisCache) MPutWithExpiration(keys []string, v interface{}, ttl int) error {
	// wrap in synchronous hystrix command
	return hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		conn.Send("MULTI")
		for i := range keys {

			var args redis.Args
			args = args.Add(keys[i])
			args = args.AddFlat(getValue(v, i))

			conn.Send("HMSET", args...)
			conn.Send("EXPIRE", keys[i], ttl)
		}
		_, err := conn.Do("EXEC")
		if err != nil {
			return errors.Wrap(err, "redis: MULTI HMSET/EXPIRE EXPIRE failed.")
		}
		return nil
	}, nil)

	return nil
}

func (c *RedisCache) MPut(keys []string, v interface{}) error {
	// wrap in synchronous hystrix command
	return hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		conn.Send("MULTI")
		for i := range keys {
			var args redis.Args
			args = args.Add(keys[i])
			args = args.AddFlat(getValue(v, i))

			conn.Send("HMSET", args...)
		}
		_, err := conn.Do("EXEC")
		if err != nil {
			return errors.Wrap(err, "redis: MULTI HMSET EXEC.")
		}
		return nil
	}, nil)
	return nil
}

type RedisGeo struct {
	Id  string
	Lng float64
	Lat float64
}

func (c *RedisCache) PutGeo(key string, geo RedisGeo) error {
	return hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		var args redis.Args
		args = args.Add(key)
		args = args.Add(geo.Lng)
		args = args.Add(geo.Lat)
		args = args.Add(geo.Id)

		_, err := conn.Do("GEOADD", args...)
		if err != nil {
			return errors.Wrapf(err, "redis: GEOADD for key %v failed", key)
		}
		return nil
	}, nil)
}

func (c *RedisCache) GetGeo(key string, members []string) ([]*RedisGeo, error) {
	geos := []*RedisGeo{}

	er := hystrix.Do(redisCommand, func() error {
		var err error
		conn := c.Pool.Get()
		defer conn.Close()

		var args redis.Args
		args = args.Add(key)
		for _, m := range members {
			args = args.Add(m)
		}
		value, err := conn.Do("GEOPOS", args...)
		if err != nil {
			return errors.Wrap(err, "redis: RECEIVE/values failed")
		}
		array := value.([]interface{})
		for i := 0; i < len(array); i++ {
			geo := &RedisGeo{}
			geo.Id = members[i]
			if array[i] == nil {
				geo = nil
			} else {
				lng, lat := extractLngLat(array[i].([]interface{}))
				geo.Lng = lng
				geo.Lat = lat
			}
			geos = append(geos, geo)
		}
		return nil
	}, nil)

	return geos, er
}

func (c *RedisCache) InGeoRange(key string, longitude float64, latitude float64, radius float64) ([]*RedisGeo, error) {
	geos := []*RedisGeo{}

	er := hystrix.Do(redisCommand, func() error {
		var err error
		conn := c.Pool.Get()
		defer conn.Close()

		var args redis.Args
		args = args.Add(key)
		args = args.Add(longitude)
		args = args.Add(latitude)
		args = args.Add(radius)
		args = args.Add("m")
		args = args.Add("WITHCOORD")
		value, err := conn.Do("GEORADIUS", args...)
		if err != nil {
			return errors.Wrap(err, "redis: GEORADIUS failed")
		}
		array := value.([]interface{})
		for i := 0; i < len(array); i++ {
			geo := &RedisGeo{}
			element := array[i].([]interface{})
			geo.Id = string(element[0].([]uint8))
			lng, lat := extractLngLat(element[1].([]interface{}))
			geo.Lng = lng
			geo.Lat = lat
			geos = append(geos, geo)
		}
		return nil
	}, nil)

	return geos, er
}

func (c *RedisCache) DeleteGeo(key string, members []string) error {
	er := hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		var args redis.Args
		args = args.Add(key)
		args = args.Add(members)

		_, err := conn.Do("ZREM", args...)
		if err != nil {
			return errors.Wrap(err, "redis: Delete failed")
		}
		return nil
	}, nil)
	return er
}

func extractLngLat(ifArray []interface{}) (float64, float64) {
	lng, _ := strconv.ParseFloat(string(ifArray[0].([]uint8)), 64)
	lat, _ := strconv.ParseFloat(string(ifArray[1].([]uint8)), 64)
	return lng, lat
}

func (c *RedisCache) ExistsSync(key string) (bool, error) {
	rc, ec := c.Exists(key)

	select {
	case exists := <-rc:
		return exists, nil

	case err := <-ec:
		return false, err
	}
}

func (c *RedisCache) Exists(key string) (chan bool, chan error) {
	rc := make(chan bool, 1)

	errors := hystrix.Go(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		exists, err := redis.Int(conn.Do("EXISTS", key))
		if err != nil {
			return errors.Wrapf(err, "redis: EXISTS for key %v failed", key)
		}
		rc <- exists == 1
		return nil
	}, nil)

	return rc, errors
}

func (c *RedisCache) MExistsSync(keys []string) ([]bool, error) {
	rc, ec := c.MExists(keys)
	output := make([]bool, len(keys))

	for i := 0; i < len(keys); i++ {
		select {
		case exists := <-rc:
			output[i] = exists
		case err := <-ec:
			return nil, err
		}
	}
	return output, nil
}

func (c *RedisCache) MExists(keys []string) (chan bool, chan error) {
	rc := make(chan bool, len(keys))

	errors := hystrix.Go(redisCommand, func() error {
		var err error
		conn := c.Pool.Get()
		defer conn.Close()

		for _, k := range keys {
			err = conn.Send("EXISTS", k)
			if err != nil {
				return errors.Wrap(err, "redis: EXISTS failed")
			}
		}
		err = conn.Flush()
		if err != nil {
			return errors.Wrap(err, "redis: FLUSH failed")
		}
		for i := 0; i < len(keys); i++ {
			exists, err := redis.Int(conn.Receive())
			if err != nil {
				return errors.Wrap(err, "redis: RECEIVE/int failed")
			}
			rc <- exists == 1
		}
		return nil
	}, nil)

	return rc, errors
}

func (c *RedisCache) PutInSet(key string, values []string) error {
	return hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		var args redis.Args
		args = args.Add(key)
		for _, v := range values {
			args = args.Add(v)
		}

		_, err := conn.Do("SADD", args...)
		if err != nil {
			return errors.Wrapf(err, "redis: SADD for key %v failed", key)
		}
		return nil
	}, nil)
}

func (c *RedisCache) GetSetMembers(key string) ([]string, error) {
	results := []string{}

	err := hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		var args redis.Args
		args = args.Add(key)

		values, err := conn.Do("SMEMBERS", args...)
		if err != nil {
			return errors.Wrapf(err, "redis: SMEMBERS for key %v failed", key)
		}
		interim := values.([]interface{})
		res := make([]string, len(interim))
		for i, v := range interim {
			res[i] = string(v.([]uint8))
		}
		results = res
		return nil
	}, nil)
	if err != nil {
		err = errors.Wrapf(err, "Failed to scan set %v", key)
	}
	return results, err
}

func (c *RedisCache) PutInSortedSet(key string, scores map[uint64]string) error {
	return hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		var args redis.Args
		args = args.Add(key)
		for k, v := range scores {
			args = args.Add(k)
			args = args.Add(v)
		}

		_, err := conn.Do("ZADD", args...)
		if err != nil {
			return errors.Wrapf(err, "redis: ZADD for key %v failed", key)
		}
		return nil
	}, nil)
}

func (c *RedisCache) RangeSortedSet(key string, scoreLow uint64, scoreHigh uint64) ([]string, error) {
	results := []string{}

	err := hystrix.Do(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		var args redis.Args
		args = args.Add(key)
		args = args.Add(scoreLow)
		args = args.Add(scoreHigh)

		values, err := conn.Do("ZRANGEBYSCORE", args...)
		if err != nil {
			return errors.Wrapf(err, "redis: ZRANGE for key %v failed", key)
		}
		interim := values.([]interface{})
		res := make([]string, len(interim))
		for i, v := range interim {
			res[i] = string(v.([]uint8))
		}
		results = res
		return nil
	}, nil)
	if err != nil {
		err = errors.Wrapf(err, "Failed to scan set %v", key)
	}
	return results, err
}

func (c *RedisCache) KeysSync(filter string) ([]string, error) {
	oc, ec := c.Keys(filter)

	select {
	case output := <-oc:
		return output, nil

	case err := <-ec:
		return nil, err
	}
}

func (c *RedisCache) Keys(filter string) (chan []string, chan error) {
	rc := make(chan []string, 1)

	errors := hystrix.Go(redisCommand, func() error {
		conn := c.Pool.Get()
		defer conn.Close()

		values, err := redis.Values(conn.Do("KEYS", filter))
		if err != nil {
			return errors.Wrapf(err, "redis: KEYS for filter %v failed", filter)
		}
		keys := make([]string, 0)
		var s string
		for _, v := range values {
			switch v.(type) {
			case string:
				s = v.(string)
			default:
				s = string(v.([]byte))
			}
			keys = append(keys, s)
		}
		rc <- keys
		return nil
	}, nil)

	return rc, errors
}

func (c *RedisCache) Clear() error {
	conn := c.Pool.Get()
	defer conn.Close()

	_, err := conn.Do("FLUSHDB")
	if err != nil {
		return errors.Wrap(err, "Clearing redis cache failed")
	}
	return nil
}

func getValue(v interface{}, i int) interface{} {
	if v == nil {
		return nil
	}

	switch reflect.TypeOf(v).Kind() {
	case reflect.Slice:
		return reflect.ValueOf(v).Index(i).Interface()
	default:
		return v
	}
}

func getPointer(v interface{}, i int) interface{} {
	if v == nil {
		return nil
	}

	switch reflect.TypeOf(v).Kind() {
	case reflect.Slice:
		return reflect.ValueOf(v).Index(i).Interface()
	default:
		return v
	}
}