package rediscache

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/elasticache"
	"github.com/pkg/errors"
	"runtime"
)

// GetClusterEndpoint returns an elasticsearch endpoint for a given cluster
func GetClusterEndpoint(cluster string) (*elasticache.Endpoint, error) {
	client := elasticache.New(session.New())

	b := true
	res, err := client.DescribeCacheClusters(&elasticache.DescribeCacheClustersInput{
		CacheClusterId:    &cluster,
		ShowCacheNodeInfo: &b,
	})

	if err != nil {
		return nil, errors.Wrapf(err, "Could not retrieve redis cluster %v.", cluster)
	}

	if len(res.CacheClusters) > 0 {
		c := res.CacheClusters[0]
		if *c.CacheClusterStatus == "available" &&
			len(c.CacheNodes) > 0 {
			n := c.CacheNodes[0]

			return n.Endpoint, nil
		}
	}

	return nil, errors.Errorf("Could not get endpoint for cluster %s", cluster)
}

func GetLocalHost() string {
	if runtime.GOOS == "windows" {
		return "127.0.0.1"
	} else {
		return "localhost"
	}
}
