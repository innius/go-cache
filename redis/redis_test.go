package rediscache

import (
	"fmt"
	"net"
	"os"
	"strings"
	"testing"
	"time"

	_ "github.com/garyburd/redigo/redis"
	"github.com/stretchr/testify/assert"
)

var host = GetLocalHost()

func sethost() {
	if h, _ := os.Hostname(); h == "C152" {
		addrs, _ := net.LookupIP(h)
		for _, addr := range addrs {
			if ipv4 := addr.To4(); ipv4 != nil {
				if strings.HasPrefix(ipv4.String(), "172.30.") {
					host = ipv4.String()
					return
				}
				if strings.HasPrefix(ipv4.String(), "192.168.") {
					host = ipv4.String()
					return
				}
			}
		}

	}
}

func TestMain(m *testing.M) {
	sethost()

	os.Exit(m.Run())
}

func TestReadWriteValue(t *testing.T) {
	c := NewRedisCache(host, 6379, 10)
	hash := "hash"
	v := "my-cached-value"
	c.SetStringValue(hash, v)

	result, err := c.GetStringValueSync(hash)

	assert.Nil(t, err)
	assert.Equal(t, v, result)
}

func TestReadExpiredHash(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)
	hash := "hash"
	v := "my-cached-value"
	c.SetStringValue(hash, v)
	time.Sleep(2 * time.Second)

	result, err := c.GetStringValueSync(hash)
	assert.Nil(t, err)
	assert.Equal(t, "", result)
}

func TestReadKeyNotInCache(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)
	value, err := c.GetStringValueSync("hash")
	assert.Nil(t, err)
	assert.Equal(t, "", value)
}

func TestReadRedisClusterNotAvailable(t *testing.T) {
	c := NewRedisCache("10.0.0.28", 6379, 1)

	_, err := c.GetStringValueSync("hash")

	assert.NotNil(t, err)
}

type (
	Machine struct {
		Id     string `redis:"id"`
		Number int    `redis:"nr"`
		State  bool   `redis:"st"`
	}
)

func TestRedisGetSync(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	key := "machine1"
	m := Machine{"M1", 10, true}
	err := c.Put(key, m)
	assert.Nil(t, err)

	m2 := &Machine{}
	err = c.GetSync(key, m2)
	fmt.Println(m2)
	assert.Nil(t, err)
	assert.Equal(t, "M1", m2.Id)
	assert.Equal(t, 10, m2.Number)
	assert.Equal(t, true, m2.State)
}

func TestRedisGet(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	key := "machine1"
	m := Machine{"M1", 10, true}
	err := c.Put(key, m)
	assert.Nil(t, err)

	m2 := &Machine{}
	rc, ec := c.Get(key, m2)
	select {
	case b := <-rc:
		assert.True(t, b)
	case err = <-ec:
		assert.Nil(t, err)
	}
	assert.Equal(t, "M1", m2.Id)
	assert.Equal(t, 10, m2.Number)
	assert.Equal(t, true, m2.State)
}

func TestRedisNilGet(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	m2 := &Machine{}
	rc, ec := c.Get("non-existing", m2)
	select {
	case b := <-rc:
		assert.False(t, b)
	case err := <-ec:
		assert.Nil(t, err)
	}
}

func TestRedisErrorGet(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	key := "machine1"
	m := Machine{"M1", 10, true}
	err := c.Put(key, m)
	assert.Nil(t, err)

	rc, ec := c.Get("machine1", nil)
	select {
	case b := <-rc:
		assert.False(t, b)
	case err := <-ec:
		assert.NotNil(t, err)
	}
}

func TestRedisNilMGet(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	keys := []string{"non-1", "non-2"}
	mm2 := []*Machine{&Machine{}, &Machine{}}

	rc, ec := c.MGet(keys, mm2)
loop:
	for i := 0; i < len(keys); i++ {
		select {
		case b := <-rc:
			assert.False(t, b)
		case err := <-ec:
			assert.Nil(t, err)
			break loop
		}
	}
}

func TestRedisErrorMGet(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	keys := []string{"machine1", "machine2"}
	mm := []Machine{{"M1", 10, true}, {"M2", 20, false}}
	err := c.MPut(keys, mm)
	assert.Nil(t, err)

	rc, ec := c.MGet(keys, nil)
loop:
	for i := 0; i < len(keys); i++ {
		select {
		case b := <-rc:
			assert.False(t, b)
		case err := <-ec:
			assert.NotNil(t, err)
			break loop
		}
	}
}

func TestRedisMGetSync(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	keys := []string{"machine1", "machine2"}
	mm := []Machine{{"M1", 10, true}, {"M2", 20, false}}
	err := c.MPut(keys, mm)
	assert.Nil(t, err)

	mm2 := []*Machine{&Machine{}, &Machine{}}

	err = c.MGetSync(keys, mm2)
	assert.Nil(t, err)
	assert.Equal(t, "M1", mm2[0].Id)
	assert.Equal(t, 10, mm2[0].Number)
	assert.Equal(t, true, mm2[0].State)
	assert.Equal(t, "M2", mm2[1].Id)
	assert.Equal(t, 20, mm2[1].Number)
	assert.Equal(t, false, mm2[1].State)
}

func TestRedisMGetNosync(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	keys := []string{"machine1", "machine2"}
	mm := []Machine{Machine{"M99", 10, true}, Machine{"M100", 20, false}}
	err := c.MPut(keys, mm)
	assert.Nil(t, err)

	// check if there is a single machine
	mx := &Machine{}
	err = c.GetSync(keys[0], mx)
	assert.Nil(t, err)
	assert.Equal(t, "M99", mx.Id)

	// check if there are multiple machines
	mm2 := []*Machine{&Machine{}, &Machine{}}
	rc, ec := c.MGet(keys, mm2)
	for i := 0; i < len(keys); i++ {
		select {
		case b := <-rc:
			assert.True(t, b)
		case err = <-ec:
			assert.Nil(t, err)
			break
		}
	}
	assert.Equal(t, "M99", mm2[0].Id)
	assert.Equal(t, 10, mm2[0].Number)
	assert.Equal(t, true, mm2[0].State)
	assert.Equal(t, "M100", mm2[1].Id)
	assert.Equal(t, 20, mm2[1].Number)
	assert.Equal(t, false, mm2[1].State)
}

func TestRedisPut(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)
	key := "machine1"
	m := Machine{"M1", 10, true}

	err := c.Put(key, m)
	assert.Nil(t, err)
}

func TestRedisPutWithExpiration(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	key := "machine1"
	m := Machine{"M22", 10, true}

	err := c.PutWithExpiration(key, m, c.ttl)
	assert.Nil(t, err)

	mx := &Machine{}
	err = c.GetSync(key, mx)
	assert.Nil(t, err)
	assert.Equal(t, "M22", mx.Id)

	time.Sleep(2 * time.Second)

	my := &Machine{}
	err = c.GetSync(key, my)
	assert.Nil(t, err)
	assert.Equal(t, "", my.Id)
}

func TestRedisMPutNoExpiration(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	keys := []string{"machine1", "machine2"}
	mm := []Machine{{"M1", 10, true}, {"M2", 20, false}}
	err := c.MPut(keys, mm)
	assert.Nil(t, err)
}

func TestRedisMPutWithExpiration(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	keys := []string{"machine1", "machine2"}
	mm := []Machine{{"M1", 10, true}, {"M2", 20, false}}
	err := c.MPutWithExpiration(keys, mm, c.ttl)
	assert.Nil(t, err)
}

func TestRedisExistsSync(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)
	key := "machine1"
	m := Machine{"M1", 10, true}
	err := c.Put(key, m)
	assert.Nil(t, err)

	exists, err := c.ExistsSync(key)
	assert.Nil(t, err)
	assert.True(t, exists)
}

func TestRedisExists(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)
	key := "machine1"
	m := Machine{"M1", 10, true}
	err := c.Put(key, m)
	assert.Nil(t, err)

	rc, ec := c.Exists(key)
	select {
	case b := <-rc:
		assert.True(t, b)
	case err = <-ec:
		assert.Nil(t, err)
	}
}

func TestRedisMExistsSync(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)
	keys := []string{"machine1", "machine2"}
	mm := []Machine{{"M1", 10, true}, {"M2", 20, false}}
	err := c.MPut(keys, mm)
	assert.Nil(t, err)

	exists, err := c.MExistsSync(append(keys, "machine3"))
	assert.Nil(t, err)
	for i := 0; i < 3; i++ {
		assert.Equal(t, (i != 2), exists[i])
	}
}

func TestRedisMExists(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)
	keys := []string{"machine1", "machine2"}
	mm := []Machine{{"M1", 10, true}, {"M2", 20, false}}
	err := c.MPut(keys, mm)
	assert.Nil(t, err)

	rc, ec := c.MExists(append(keys, "machine3"))
	for i := 0; i < len(keys); i++ {
		select {
		case b := <-rc:
			assert.Equal(t, (i != 2), b)
		case err := <-ec:
			assert.Nil(t, err)
			break
		}
	}
}

func TestRedisKeys(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	key := "m1:p1"
	m := Machine{"M1", 10, true}
	c.Put(key, m)

	key = "m1:p2"
	m = Machine{"M2", 20, false}
	c.Put(key, m)

	keys, err := c.KeysSync("m1:*")
	assert.Nil(t, err)
	assert.NotNil(t, keys)
	assert.Equal(t, 2, len(keys))
}

func TestRedisGeoAddGet(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	key := "geoMachines"
	geo := RedisGeo{
		Id:  "to-increase",
		Lat: 52.045054,
		Lng: 5.564473,
	}

	err := c.PutGeo(key, geo)
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}

	geos, err := c.GetGeo(key, []string{"to-increase"})
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}

	geoGet := geos[0]
	assert.Equal(t, "to-increase", geoGet.Id)
	assert.True(t, geo.Lat-0.001 < geoGet.Lat && geoGet.Lat < geo.Lat+0.001) //Redis makes it longer, so some error margin is allowed
	assert.True(t, geo.Lng-0.001 < geoGet.Lng && geoGet.Lng < geo.Lat+0.001)

	err = c.DeleteGeo(key, []string{"to-increase"})
	assert.Nil(t, err)
}

func TestRedisGeoGetMulti(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	key := "geoMachines"
	geo1 := RedisGeo{
		Id:  "to-increase",
		Lat: 52.045054,
		Lng: 5.564473,
	}
	geo2 := RedisGeo{
		Id:  "station",
		Lat: 52.046044,
		Lng: 5.573410,
	}

	err := c.PutGeo(key, geo1)
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}

	err = c.PutGeo(key, geo2)
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}

	geos, err := c.GetGeo(key, []string{"to-increase", "station"})
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}

	assert.Equal(t, 2, len(geos))
	assert.Equal(t, "to-increase", geos[0].Id)
	assert.True(t, geo1.Lat-0.001 < geos[0].Lat && geos[0].Lat < geos[0].Lat+0.001) //Redis makes it longer, so some error margin is allowed
	assert.True(t, geo1.Lng-0.001 < geos[0].Lng && geos[0].Lng < geos[0].Lat+0.001)

	assert.Equal(t, "station", geos[1].Id)
	assert.True(t, geo1.Lat-0.001 < geos[1].Lat && geos[1].Lat < geo2.Lat+0.001) //Redis makes it longer, so some error margin is allowed
	assert.True(t, geo1.Lng-0.001 < geos[1].Lng && geos[1].Lng < geo2.Lat+0.001)

	err = c.DeleteGeo(key, []string{"to-increase", "station"})
	assert.Nil(t, err)
}

func TestRedisGeoRadius(t *testing.T) {
	c := NewRedisCache(host, 6379, 1)

	key := "geoMachines"
	geo1 := RedisGeo{
		Id:  "to-increase",
		Lat: 52.045054,
		Lng: 5.564473,
	}
	geo2 := RedisGeo{
		Id:  "station",
		Lat: 52.046044,
		Lng: 5.573410,
	}

	err := c.PutGeo(key, geo1)
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}

	err = c.PutGeo(key, geo2)
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}

	geos, err := c.InGeoRange(key, 5.564473, 52.045054, 1000)
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}

	assert.Equal(t, 2, len(geos))
	assert.Equal(t, "to-increase", geos[0].Id)
	assert.True(t, geo1.Lat-0.001 < geos[0].Lat && geos[0].Lat < geos[0].Lat+0.001) //Redis makes it longer, so some error margin is allowed
	assert.True(t, geo1.Lng-0.001 < geos[0].Lng && geos[0].Lng < geos[0].Lat+0.001)

	assert.Equal(t, "station", geos[1].Id)
	assert.True(t, geo1.Lat-0.001 < geos[1].Lat && geos[1].Lat < geo2.Lat+0.001) //Redis makes it longer, so some error margin is allowed
	assert.True(t, geo1.Lng-0.001 < geos[1].Lng && geos[1].Lng < geo2.Lat+0.001)

	err = c.DeleteGeo(key, []string{"to-increase", "station"})
	assert.Nil(t, err)
}

func TestPutSetAndRange(t *testing.T) {
	input := make(map[uint64]string)
	input[0] = "to-increase"
	input[11] = "station"
	input[714] = "joop van zanten"

	key := "SetTest"

	c := NewRedisCache(host, 6379, 1)

	err := c.PutInSortedSet(key, input)
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}
	results, err := c.RangeSortedSet(key, 0, 500)
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}
	assert.Equal(t, 2, len(results))
	assert.Equal(t, "to-increase", results[0])
	assert.Equal(t, "station", results[1])
}

func TestUnsortedSet(t *testing.T) {
	input := []string{"Hello", "World"}
	key := "UnsortedSetTest"

	c := NewRedisCache(host, 6379, 1)

	err := c.PutInSet(key, input)
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}
	results, err := c.GetSetMembers(key)
	assert.Nil(t, err)
	if err != nil {
		t.Fail()
	}

	cond := func(val string) bool {return val == "Hello" || val == "World"}
	assert.Equal(t, 2, len(results))
	assert.True(t, cond(results[0]))
	assert.True(t, cond(results[1]))
	assert.True(t, results[0] != results[1])
}

const BIG = 5000

func Benchmark5KPut(b *testing.B) {
	c := NewRedisCache(host, 6379, 1)
	c.Clear()
	k, m := makeMachines()

	for it := 0; it < b.N; it++ {
		for i := range k {
			c.Put(k[i], m[i])
		}
	}
}
func Benchmark5KMPut(b *testing.B) {
	c := NewRedisCache(host, 6379, 1)
	c.Clear()

	k, m := makeMachines()
	for it := 0; it < b.N; it++ {
		c.MPut(k, m)
	}
}
func Benchmark5KGet(b *testing.B) {
	c := NewRedisCache(host, 6379, 1)
	c.Clear()
	k, m := makeMachines()
	c.MPut(k, m)

	mm := &Machine{}
	for it := 0; it < b.N; it++ {
	loop:
		for i := range k {
			rc, ec := c.Get(k[i], mm)
			select {
			case <-rc:
			case <-ec:
				break loop
			}
		}
	}
}
func Benchmark5KMGet(b *testing.B) {
	c := NewRedisCache(host, 6379, 1)
	c.Clear()
	k, m := makeMachines()
	c.MPut(k, m)

	mm := makeMachineStructs()
	for it := 0; it < b.N; it++ {
		rc, ec := c.MGet(k, mm)
	loop:
		for i := 0; i < len(k); i++ {
			select {
			case <-rc:
			case <-ec:
				break loop
			}
		}
	}
}
func makeMachines() ([]string, []Machine) {
	keys := make([]string, BIG)
	machines := make([]Machine, BIG)
	for i := 0; i < BIG; i++ {
		keys[i] = fmt.Sprintf("machine-%d", i)
		machines[i] = Machine{keys[i], i, true}
	}
	return keys, machines
}
func makeMachineStructs() []*Machine {
	machines := make([]*Machine, BIG)
	for i := 0; i < BIG; i++ {
		machines[i] = &Machine{}
	}
	return machines
}
