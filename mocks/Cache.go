package cache

import "github.com/stretchr/testify/mock"

import "bitbucket.org/innius/go-cache/redis"

type MockCache struct {
	mock.Mock
}

// SetStringValue provides a mock function with given fields: _a0, _a1
func (_m *MockCache) SetStringValue(_a0 string, _a1 string) error {
	ret := _m.Called(_a0, _a1)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, string) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetStringValue provides a mock function with given fields: _a0
func (_m *MockCache) GetStringValue(_a0 string) (chan string, chan error) {
	ret := _m.Called(_a0)

	var r0 chan string
	if rf, ok := ret.Get(0).(func(string) chan string); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(chan string)
		}
	}

	var r1 chan error
	if rf, ok := ret.Get(1).(func(string) chan error); ok {
		r1 = rf(_a0)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).(chan error)
		}
	}

	return r0, r1
}

// GetStringValueSync provides a mock function with given fields: _a0
func (_m *MockCache) GetStringValueSync(_a0 string) (string, error) {
	ret := _m.Called(_a0)

	var r0 string
	if rf, ok := ret.Get(0).(func(string) string); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetSync provides a mock function with given fields: key, v
func (_m *MockCache) GetSync(key string, v interface{}) error {
	ret := _m.Called(key, v)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, interface{}) error); ok {
		r0 = rf(key, v)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Get provides a mock function with given fields: key, v
func (_m *MockCache) Get(key string, v interface{}) (chan bool, chan error) {
	ret := _m.Called(key, v)

	var r0 chan bool
	if rf, ok := ret.Get(0).(func(string, interface{}) chan bool); ok {
		r0 = rf(key, v)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(chan bool)
		}
	}

	var r1 chan error
	if rf, ok := ret.Get(1).(func(string, interface{}) chan error); ok {
		r1 = rf(key, v)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).(chan error)
		}
	}

	return r0, r1
}

// MGetSync provides a mock function with given fields: keys, vv
func (_m *MockCache) MGetSync(keys []string, vv interface{}) error {
	ret := _m.Called(keys, vv)

	var r0 error
	if rf, ok := ret.Get(0).(func([]string, interface{}) error); ok {
		r0 = rf(keys, vv)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// MGet provides a mock function with given fields: keys, vv
func (_m *MockCache) MGet(keys []string, vv interface{}) (chan bool, chan error) {
	ret := _m.Called(keys, vv)

	var r0 chan bool
	if rf, ok := ret.Get(0).(func([]string, interface{}) chan bool); ok {
		r0 = rf(keys, vv)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(chan bool)
		}
	}

	var r1 chan error
	if rf, ok := ret.Get(1).(func([]string, interface{}) chan error); ok {
		r1 = rf(keys, vv)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).(chan error)
		}
	}

	return r0, r1
}

// Put provides a mock function with given fields: key, v
func (_m *MockCache) Put(key string, v interface{}) error {
	ret := _m.Called(key, v)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, interface{}) error); ok {
		r0 = rf(key, v)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// PutWithExpiration provides a mock function with given fields: key, v, expire
func (_m *MockCache) PutWithExpiration(key string, v interface{}, expire int) error {
	ret := _m.Called(key, v, expire)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, interface{}, int) error); ok {
		r0 = rf(key, v, expire)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// MPut provides a mock function with given fields: keys, vv
func (_m *MockCache) MPut(keys []string, vv interface{}) error {
	ret := _m.Called(keys, vv)

	var r0 error
	if rf, ok := ret.Get(0).(func([]string, interface{}) error); ok {
		r0 = rf(keys, vv)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// MPutWithExpiration provides a mock function with given fields: keys, vv, expire
func (_m *MockCache) MPutWithExpiration(keys []string, vv interface{}, expire int) error {
	ret := _m.Called(keys, vv, expire)

	var r0 error
	if rf, ok := ret.Get(0).(func([]string, interface{}, int) error); ok {
		r0 = rf(keys, vv, expire)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ExistsSync provides a mock function with given fields: key
func (_m *MockCache) ExistsSync(key string) (bool, error) {
	ret := _m.Called(key)

	var r0 bool
	if rf, ok := ret.Get(0).(func(string) bool); ok {
		r0 = rf(key)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(key)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Exists provides a mock function with given fields: key
func (_m *MockCache) Exists(key string) (chan bool, chan error) {
	ret := _m.Called(key)

	var r0 chan bool
	if rf, ok := ret.Get(0).(func(string) chan bool); ok {
		r0 = rf(key)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(chan bool)
		}
	}

	var r1 chan error
	if rf, ok := ret.Get(1).(func(string) chan error); ok {
		r1 = rf(key)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).(chan error)
		}
	}

	return r0, r1
}

// MExistsSync provides a mock function with given fields: keys
func (_m *MockCache) MExistsSync(keys []string) ([]bool, error) {
	ret := _m.Called(keys)

	var r0 []bool
	if rf, ok := ret.Get(0).(func([]string) []bool); ok {
		r0 = rf(keys)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]bool)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func([]string) error); ok {
		r1 = rf(keys)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// MExists provides a mock function with given fields: keys
func (_m *MockCache) MExists(keys []string) (chan bool, chan error) {
	ret := _m.Called(keys)

	var r0 chan bool
	if rf, ok := ret.Get(0).(func([]string) chan bool); ok {
		r0 = rf(keys)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(chan bool)
		}
	}

	var r1 chan error
	if rf, ok := ret.Get(1).(func([]string) chan error); ok {
		r1 = rf(keys)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).(chan error)
		}
	}

	return r0, r1
}

// PutGeo provides a mock function with given fields: key, geo
func (_m *MockCache) PutGeo(key string, geo rediscache.RedisGeo) error {
	ret := _m.Called(key, geo)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, rediscache.RedisGeo) error); ok {
		r0 = rf(key, geo)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetGeo provides a mock function with given fields: key, members
func (_m *MockCache) GetGeo(key string, members []string) ([]*rediscache.RedisGeo, error) {
	ret := _m.Called(key, members)

	var r0 []*rediscache.RedisGeo
	if rf, ok := ret.Get(0).(func(string, []string) []*rediscache.RedisGeo); ok {
		r0 = rf(key, members)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*rediscache.RedisGeo)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, []string) error); ok {
		r1 = rf(key, members)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// InGeoRange provides a mock function with given fields: key, longitude, latitude, radius
func (_m *MockCache) InGeoRange(key string, longitude float64, latitude float64, radius float64) ([]*rediscache.RedisGeo, error) {
	ret := _m.Called(key, longitude, latitude, radius)

	var r0 []*rediscache.RedisGeo
	if rf, ok := ret.Get(0).(func(string, float64, float64, float64) []*rediscache.RedisGeo); ok {
		r0 = rf(key, longitude, latitude, radius)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*rediscache.RedisGeo)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, float64, float64, float64) error); ok {
		r1 = rf(key, longitude, latitude, radius)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// PutInSet provides a mock function with given fields: key, values
func (_m *MockCache) PutInSet(key string, values []string) error {
	ret := _m.Called(key, values)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, []string) error); ok {
		r0 = rf(key, values)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetSetMembers provides a mock function with given fields: key
func (_m *MockCache) GetSetMembers(key string) ([]string, error) {
	ret := _m.Called(key)

	var r0 []string
	if rf, ok := ret.Get(0).(func(string) []string); ok {
		r0 = rf(key)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(key)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// PutInSortedSet provides a mock function with given fields: key, scores
func (_m *MockCache) PutInSortedSet(key string, scores map[uint64]string) error {
	ret := _m.Called(key, scores)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, map[uint64]string) error); ok {
		r0 = rf(key, scores)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// RangeSortedSet provides a mock function with given fields: key, scoreLow, scoreHigh
func (_m *MockCache) RangeSortedSet(key string, scoreLow uint64, scoreHigh uint64) ([]string, error) {
	ret := _m.Called(key, scoreLow, scoreHigh)

	var r0 []string
	if rf, ok := ret.Get(0).(func(string, uint64, uint64) []string); ok {
		r0 = rf(key, scoreLow, scoreHigh)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, uint64, uint64) error); ok {
		r1 = rf(key, scoreLow, scoreHigh)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// KeysSync provides a mock function with given fields: filter
func (_m *MockCache) KeysSync(filter string) ([]string, error) {
	ret := _m.Called(filter)

	var r0 []string
	if rf, ok := ret.Get(0).(func(string) []string); ok {
		r0 = rf(filter)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(filter)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Keys provides a mock function with given fields: filter
func (_m *MockCache) Keys(filter string) (chan []string, chan error) {
	ret := _m.Called(filter)

	var r0 chan []string
	if rf, ok := ret.Get(0).(func(string) chan []string); ok {
		r0 = rf(filter)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(chan []string)
		}
	}

	var r1 chan error
	if rf, ok := ret.Get(1).(func(string) chan error); ok {
		r1 = rf(filter)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).(chan error)
		}
	}

	return r0, r1
}

// Ping provides a mock function with given fields:
func (_m *MockCache) Ping() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Clear provides a mock function with given fields:
func (_m *MockCache) Clear() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
